package com.example.girls

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI
/**
 * 该app简单的使用jetPack库MVVM架构进行开发
 * 一个activity多个fragment利用navigation组件进行导航
 * */

class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // 设置navController from fragment id
        navController = Navigation.findNavController(this, R.id.fragment)
        // 显示导航标签
        NavigationUI.setupActionBarWithNavController(this,findNavController(R.id.fragment))
    }

    override fun onSupportNavigateUp(): Boolean {
        return super.onSupportNavigateUp() || navController.navigateUp()
    }
}