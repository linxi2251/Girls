package com.example.girls.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

data class Girl(
    val page_count: Int,
    val data: Array<PhotoItem>,
    val page:Int,
    val status: Int,
    val total_counts: Int
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Girl

        if (page_count != other.page_count) return false
        if (!data.contentEquals(other.data)) return false
        if (page != other.page) return false
        if (status != other.status) return false
        if (total_counts != other.total_counts) return false

        return true
    }

    override fun hashCode(): Int {
        var result = page_count
        result = 31 * result + data.contentHashCode()
        result = 31 * result + page
        result = 31 * result + status
        result = 31 * result + total_counts
        return result
    }
}

@Parcelize
data class PhotoItem(
    val url: String,
    val _id: String
): Parcelable